let ctx = document.getElementById('barchart').getContext('2d');
let barChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [],
    datasets: [{
      label: '# score',
      data: [],
      backgroundColor: [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)',

      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',

      ],
      borderWidth: 1
    }]
  },
  options: {

    responsive: true,
    maintainAspectRatio: false,

    scales: {
      y: {
        beginAtZero: true
      }
    }
  }

});
let ctx2 = document.getElementById('piechart').getContext('2d');
let pieChart = new Chart(ctx2, {
  type: 'pie',
  data: {
    labels: [],
    datasets: [{
      label: '# score',
      data: [],
      backgroundColor: [
        'rgba(255, 99, 132, 0.5)',
        'rgba(54, 162, 235, 0.5)',
        'rgba(255, 206, 86, 0.5)',
        'rgba(75, 192, 192, 0.5)',
        'rgba(153, 102, 255, 0.5)'

      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',

      ],
      borderWidth: 1
    }]
  },
  options: {
    responsive: true,
    maintainAspectRatio: false,

    scales: {
      y: {
        beginAtZero: true
      }
    }
  }

});

function barchart(lables, data) {
  barChart.data.labels = lables;
  barChart.data.datasets[0].data = data;

  barChart.update();

}
function piechart(lables, data) {
  pieChart.data.labels = lables;
  pieChart.data.datasets[0].data = data;

  pieChart.update();
}


async function first() {

  let data = await (await fetch("getdata/")).json();

  barchart(data.lable, data.score)
  piechart(data.lable, data.score)
}
first();

const changechartbar = async (id) => {

  const flag = document.getElementById(id).value;
  const scores = await (await fetch("studata?id=" + flag)).json();
  const data = scores.score;
  const label = scores.lable;

  barchart(label, data);


}

const changechartpie = async (id) => {

  const flag = document.getElementById(id).value;

  const scores = await (await fetch("studata?id=" + flag)).json();
  const data = scores.score;
  const label = scores.lable;
  piechart(label, data);


}

