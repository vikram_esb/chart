const express = require('express');
const app = express();
const path = require('path');
const port = 5000;
const mysql = require('mysql2');
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "chart"
});

app.use(express.static(path.join(__dirname, 'public')));

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "src/views"));

app.use(express.urlencoded({ extended: false }));

app.listen(port, () => {
  console.log(`Server is up on http://localhost:${port}/`);
});

app.get('/', (req, res) => {

  try {
    connection.query(`select * from student_master`, (err, students) => {
      if (err) throw err;
      else {
        // console.log(students);
        res.render('chart', { students });
      }

    })

  } catch (error) {
    console.log(error.message);
  }
})


app.get("/getdata", (req, res) => {
  try {
    connection.query(`SELECT subject,avg(score) as average  FROM student_subject_master GROUP BY subject;`, (err, result) => {

      // console.log(result);   
      let score = [];
      let lable = [];
      result.forEach((e) => {
        score.push(e.average);
        lable.push(e.subject);
      })

      res.send({ score, lable });

    })


  } catch (error) {
    console.log(error.message);
  }
})


app.get("/studata", (req, res) => {
  try {
    id = req.query.id;


    if (id == 'all') {

      res.redirect('/getdata');
    }
    else {
      connection.query(`select * from student_subject_master where stu_id= '${id}'`, (err, result) => {
        // console.log(result);
        let score = [];
        let lable = [];
        result.forEach((e) => {
          score.push(e.score);
          lable.push(e.subject);
        })

        res.send({ score, lable })

      })
    }
  } catch (error) {
    console.log(error.message);
  }
})